#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function install_helm(){
    log_info "Mise à jour des paquets et depot"
    sudo apt update
    
    log_info "Installation de Helm"
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    chmod 700 get_helm.sh
    ./get_helm.sh

    log_info "version de Helm"
    helm version

    log_success "Installation de Helm terminée"
}
echo -e "\n\033[34m ################################# This is a script to install Helm ################################# \033[0m\n\n"

if command -v helm &> /dev/null; then
    log_info "Helm already installed "
    log_info "Goodbye 😀"
else
    install_helm
fi