#!/bin/bash

set -e

# namespace="longhorn-system"
# expected_pod_count=$(kubectl get pods -n "$namespace" --no-headers | wc -l)

# function get_pod_status() {
#     kubectl get pods -n "$namespace" --no-headers | awk '{print $3}'
# }

# function wait_for_pods_ready() {
#     echo "Waiting for pods in $namespace namespace to be ready..."

#     while true; do
#         pod_status=$(get_pod_status)
#         ready_count=$(echo "$pod_status" | grep -cE '^Running|^Completed')

#         if [ "$ready_count" -eq "$expected_pod_count" ]; then
#             echo "All pods are ready."
#             break
#         else
#             echo "Waiting for pods to be ready. Current status: "
#             kubectl get pods -n "$namespace"
#             sleep 5
#         fi
#     done
# }




function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }


function install_longhorn(){
    log_info "Ajout du repository Helm de Longhorn Helm"
    helm repo add longhorn https://charts.longhorn.io

    log_info "Récupère les derniers charts depuis le repository"
    helm repo update

    log_info "Affichage des version de longhorn"
    helm search repo longhorn/longhorn --versions

    log_info "Installation de longhorn"
    helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --version 1.5.3 --set persistence.defaultClassReplicaCount=1,longhornUI.replicas=1

    log_info " Affichage des pods de Longhorn "
    kubectl -n longhorn-system get pod

    log_success "Installation de Longhorn terminée. NB: Attendez que tous les pods soient prêt"
}
echo -e "\n\033[34m ################################# This is a script to install Longhorn on k8s cluster ################################# \033[0m\n\n"
install_longhorn