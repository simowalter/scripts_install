#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function install_docker(){
    log_info "Mise à jour des paquets et depot"
    sudo apt update
    sudo apt upgrade -y
    sudo apt-get install ca-certificates curl gnupg lsb-release -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor \
    -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo \
    "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y

    log_info "Installation de Docker-compose"
    sudo apt install docker-compose
    sudo adduser $USER docker
    sudo usermod -aG docker $USER

    log_info "Reload of deamon docker and restart of Docker"
    sudo systemctl daemon-reload
    sudo systemctl restart docker

    log_success "Installation de Docker terminée"
}

function install_minikube(){
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    sudo install minikube-linux-amd64 /usr/local/bin/minikube
    minikube start

    log_info "Minikube installed. Display of the version of minikube"
    minikube version

    log_info "Display of pods of minikube"
    minikube kubectl -- get pods -A

    log_info "Installation de kubectl"
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

    log_info "Test kubectl"

    log_info "Info API kubectl"
    kubectl api-resources

    log_info "node info"
    kubectl get node -o wide

    log_info "All pods"
    kubectl get pods --all-namespaces

    log_success "Installation de minikube et kubectl terminée "
}

function uninstall_minikube(){
    minikube stop
    minikube delete
}
echo -e "\n\033[34m ################################# This is a script to install Minikube & kubectl ################################# \033[0m\n\n"

log_info "Verification de l'installation de docker (requis)"
if command -v docker &> /dev/null; then
    log_info "Docker already installed 😀"
else
    log_info "Docker not already installed 😕"
    log_info "Installation de Docker"
    install_docker
fi

if command -v docker &> /dev/null; then
    log_info "Docker already installed "
    read -p "---------------- Do you want to purge and reinstall? type 'y' or 'n' : " response

    if [ "$response" = "y" ]; then
        uninstall_minikube
        install_minikube
    else
        log_info "Goodbye 😀"
    fi
else
    install_minikube
fi