#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function install_git(){
    #install git
    sudo apt install git-all
    #check git version
    git --version

    log_success "Git successfully installed"
}

echo -e "\n\033[34m ################################# This is a script to install GIT ################################# \033[0m\n\n"

log_info "Mise à jour des paquets et depot"
# update and upgrade of packages
sudo apt update
sudo apt upgrade -y

log_info "Installation wget, curl, vim, nano, tree, w3m"
# essential packages install
sudo apt -y install wget w3m curl vim nano tree

if command -v git &> /dev/null; then
    log_info "GIt already installed "
    log_info "Goodbye 😀"
else
    log_info "Installation de GIT"
    install_git
fi