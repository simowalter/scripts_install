#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function install_docker(){
    log_info "Mise à jour des paquets et depot"
    sudo apt update
    sudo apt upgrade -y
    sudo apt-get install ca-certificates curl gnupg lsb-release -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor \
    -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo \
    "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y

    log_info "Installation de Docker-compose"
    sudo apt install docker-compose
    sudo adduser $USER docker
    sudo usermod -aG docker $USER

    log_info "Reload of deamon docker and restart of Docker"
    sudo systemctl daemon-reload
    sudo systemctl restart docker

    log_success "Installation de Docker terminée"
}

function run_rancher(){

    log_info "Pay attention. You will need to provide password for sudo user!"
    sudo docker run -d \
    --restart=unless-stopped \
    -p 8080:80 \
    -p 8443:443 \
    -v /opt/rancher:/var/lib/rancher \
    --name rancher \
    --privileged rancher/rancher:stable

    log_info "Follow the link https://localhost:8443 and follow the instructions to login and access the Rancher Dashboard"
    log_info "Then copy the KUBECONFIG and export to wherever you want to"
}


echo -e "\n\033[34m ################################# This is a script to run k8s cluster using Rancher ################################# \033[0m\n\n"

log_info "Verification de l'installation de docker (requis)"
if command -v docker &> /dev/null; then
    log_info "Docker already installed 😀"
else
    log_info "Docker not already installed 😕"
    log_info "Installation de Docker"
    install_docker
fi

run_rancher