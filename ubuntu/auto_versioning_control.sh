#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function log_error() {
    echo -e "\n\e[97;41m[ERROR] 😞😞 $*  😞😞  \e[0m \n\n"
}

# Function to log warnings in orange
function log_warning() {
    echo -e "\n\e[38;5;202m[WARNING] ⚠️⚠️ $* ⚠️⚠️ \e[0m \n\n"
}

# Check if the current directory is a Git repository
if ! git rev-parse --is-inside-work-tree &> /dev/null; then
    log_error "The current directory is not a Git repository. You should execute this script in the Git repository !"
    exit 1
fi

log_info "Verification that we are in a Git repository successful."

if ! command -v node &> /dev/null || [[ "$(node -v)" != v1[4-9].* && "$(node -v)" != v20.* ]]; then
    log_info "Node.js is not installed or version is outside the range 14-20, install it"
    curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -
    sudo apt-get install -y nodejs
else
    log_info "Node.js is already installed and its version is within the range 14-20."
fi

log_info "Installation of the librairy husky"
npm install husky --save-dev

log_info "activate the hooks of husky:"

npx husky install

log_info "Adding the command 'prepare': 'husky install' in the scripts"

npm pkg set scripts.prepare="husky install"

log_info "installation of librairy commitlint and configuration of our convention "

npm install @commitlint/cli @commitlint/config-conventional --save-dev

# set that we want to use “Conventional Commits” :
cat <<EOF > commitlint.config.js
module.exports = {
  extends: ['@commitlint/config-conventional']
};
EOF

# Add our hook Git via husky 
npx husky add .husky/commit-msg  "npx --no -- commitlint --edit ${1}"

log_info "installing standard-version for the auto management of tag and CHANGELOG.md"

npm install --save-dev standard-version

log_info "Adding the command 'release': 'standard-version' in the scripts"
npm pkg set scripts.release="standard-version"
npm pkg set scripts.minor_release="standard-version --release-as minor"
npm pkg set scripts.major_release="standard-version --release-as major"

if ! command -v jq &> /dev/null; then
  log_info "jq is not installed. Installing..."
  if [[ "$OSTYPE" == "darwin"* ]]; then
    brew install -y jq
  else
    sudo apt-get update
    sudo apt-get install -y jq
  fi
fi

PACKAGE_JSON="./package.json"
INIT_VERSION="0.0.1"

if ! jq -e '.version' "$PACKAGE_JSON" > /dev/null; then
  log_info "Setting version to $INIT_VERSION"
  npm version "$INIT_VERSION" --no-git-tag-version
else
  CURRENT_VERSION=$(jq -r '.version' "$PACKAGE_JSON")
  log_info "Version already exists: $CURRENT_VERSION"
fi

log_info "At the moment of making a new release, run 'npm run release'"


log_info "Update of the .gitignore file "

gitignore_file=".gitignore"
entries_to_add=".husky
node_modules"

# Convert entries_to_add into an array
IFS=$'\n' read -r -d '' -a entries_array <<< "$entries_to_add"

# Check if .gitignore exists
if [ -f "$gitignore_file" ]; then
    # Check and add each entry separately
    for entry in "${entries_array[@]}"; do
        if grep -Fxq "$entry" "$gitignore_file"; then
            log_info "Entry $entry already exists in $gitignore_file"
        else
            # Append entry to .gitignore
            log_info "$entry" >> "$gitignore_file"
            log_info "Added entry $entry to $gitignore_file"
        fi
    done
else
    # Create .gitignore and add entries
    log_info "$entries_to_add" > "$gitignore_file"
    log_info ".gitignore created and entries added"
fi

log_success "Installation complete"

log_info "At the moment of making a new release, run 'npm run release'"

log_info "For releasing a new version with MAJOR incremented, You can run the command 'npm run major_release' (or 'npm run release -- --release-as major')."

log_info "For releasing a new version with MINOR incremented,  You can run the command 'npm run minor_release' (or 'npm run release -- --release-as minor')."

log_warning "When the version of the release X.Y.Z is such that X<1, You should use 'npm run minor_release' or 'npm run major_release' for minor(Y) and major(X) increment "

log_info "This script facilitates automated updates to the CHANGELOG.md file and the creation of new tags, adhering to the principles of semantic versioning( https://semver.org/)."

log_info "To push the tags to the distant repo, run: 'git push origin --tags'"