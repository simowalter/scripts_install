#!/bin/bash

set -e


function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function install_ansible(){
    log_info "Mise à jour des paquets et depot"
    sudo apt-get update
    sudo apt-get install software-properties-common
    sudo apt-add-repository ppa:ansible/ansible
    sudo apt-get update
    sudo apt-get install ansible -y

    log_success "Installation de Ansible terminée"
}


if command -v ansible &> /dev/null; then
    log_info "Ansible already installed "
else
    install_ansible
fi