#!/bin/bash

set -e

echo -e "\n\033[34m ******************************************************************     \033[0m\n\n"
echo -e "\n\033[34m ******************** II - INSTALLATION HARBOR ********************     \033[0m\n\n"
echo -e "\n\033[34m ******************************************************************     \033[0m\n\n"

echo -e "\n\033[34m -------------------------------> Download of the Harbor helm chart \033[0m\n\n"
helm repo add bitnami https://charts.bitnami.com/bitnami
helm fetch bitnami/harbor --untar

read -p " Voulez vous considerez les valeurs par defaut dans values.yml ? [y/n] " edit_values
if [[ $edit_values =~ ^[nN]$ ]]; then
    helm show values harbor > harbor-values-bitnami.yml
    read -p "Choisissez votre éditeur de texte préféré (nano/vim) : " editor_choice
    case "$editor_choice" in
        "nano")
            nano harbor-values-bitnami.yml
            ;;
        "vim")
            vim harbor-values-bitnami.yml
            ;;
        *)
            echo "Choix invalide. Le fichier sera installé avec les valeurs par défaut."
            ;;
    esac
    echo -e "\n\033[34m -------------------------------> Installation of Harbor Bitnami chart \033[0m\n\n"

    helm_version=$(helm version --short | awk -F'[+.]' '{print $1}')
    if [ "$helm_version" == "v2" ]; then
    helm install harbor --values harbor-values-bitnami.yml --name harbor-bitnami --namespace harbor 
    elif [ "$helm_version" == "v3" ]; then
    helm install harbor-bitnami harbor  --values harbor-values-bitnami.yml --namespace harbor --create-namespace 
    fi
else
    # Prompt user for password (without echoing to console)
    read -s -p "\nEnter adminPassword for HARBOR UI: " ADMIN_PASSWORD_HARBOR_UI

    read -p "\nEnter domaine name for HARBOR registry: " DOMAIN

    echo -e "\n\033[34m -------------------------------> Installation of harbor chart \033[0m\n\n"

    read -p "\n Voulez vous le mode TLS ? [y/n] " enable_tls
    if [[ $enable_tls =~ ^[yY]$ ]]; then
        helm_version=$(helm version --short | awk -F'[+.]' '{print $1}')
        if [ "$helm_version" == "v2" ]; then
            helm install harbor --name harbor-bitnami --namespace harbor --set adminPassword=$ADMIN_PASSWORD_HARBOR_UI,service.type=NodePort,service.nodePorts.http=30080,service.nodePorts.https=30443,externalURL=https://$DOMAIN,nginx.tls.commonName=$DOMAIN,persistence.persistentVolumeClaim.trivy.size="2Gi",trivy.resources.requests.cpu="100m",trivy.resources.requests.memory="128Mi",trivy.resources.limits.cpu="200m",trivy.resources.limits.memory="256Mi"
        elif [ "$helm_version" == "v3" ]; then
            helm install harbor-bitnami harbor  --namespace harbor --create-namespace --set adminPassword=$ADMIN_PASSWORD_HARBOR_UI,service.type=NodePort,service.nodePorts.http=30080,service.nodePorts.https=30443,externalURL=https://$DOMAIN,nginx.tls.commonName=$DOMAIN,persistence.persistentVolumeClaim.trivy.size="2Gi",trivy.resources.requests.cpu="100m",trivy.resources.requests.memory="128Mi",trivy.resources.limits.cpu="200m",trivy.resources.limits.memory="256Mi"
        fi
    else 
        helm_version=$(helm version --short | awk -F'[+.]' '{print $1}')
        if [ "$helm_version" == "v2" ]; then
            helm install harbor --name harbor-bitnami --namespace harbor --set adminPassword=$ADMIN_PASSWORD_HARBOR_UI,nginx.tls.enabled=false,service.type=NodePort,service.nodePorts.http=30080,service.nodePorts.https=30443,externalURL=https://$DOMAIN,nginx.tls.commonName=$DOMAIN,persistence.persistentVolumeClaim.trivy.size="2Gi",trivy.resources.requests.cpu="100m",trivy.resources.requests.memory="128Mi",trivy.resources.limits.cpu="200m",trivy.resources.limits.memory="256Mi"
        elif [ "$helm_version" == "v3" ]; then
            helm install harbor-bitnami harbor  --namespace harbor --create-namespace --set adminPassword=$ADMIN_PASSWORD_HARBOR_UI,service.type=NodePort,service.nodePorts.http=30080,service.nodePorts.https=30443,externalURL=https://$DOMAIN,nginx.tls.commonName=$DOMAIN,nginx.tls.enabled=false,persistence.persistentVolumeClaim.trivy.size="2Gi",trivy.resources.requests.cpu="100m",trivy.resources.requests.memory="128Mi",trivy.resources.limits.cpu="200m",trivy.resources.limits.memory="256Mi"
        fi
    fi
fi