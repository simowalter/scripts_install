#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function generate_key_fullchain_cert(){
    log_info "Mise à jour des paquets et depot"
    sudo apt-get update
    log_info "installation Certbot"
    sudo apt install certbot
    log_info "Creation certificats et clés privées"
    sudo certbot --manual --preferred-challenges dns certonly -d $1
}
    

generate_key_fullchain_cert my-domain.com
