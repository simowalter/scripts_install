#!/bin/bash

set -e


function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }


log_info "Save the old iptables setting to ~/iptables-rules"

## save existing rules
sudo iptables-save > ~/iptables-rules

log_info "Remove DROP rules from the iptables setting "
## modify rules, remove drop and reject lines
grep -v "DROP" iptables-rules > tmpfile && mv tmpfile iptables-rules-mod

log_info "Remove REJECT rules from the iptables setting "
grep -v "REJECT" iptables-rules-mod > tmpfile && mv tmpfile iptables-rules-mod

## apply the modifications

log_info "Apply modifications of the iptables setting "
sudo iptables-restore < ~/iptables-rules-mod

log_info "Display the rules after modification to check "
## check
sudo iptables -L

log_info "Save the changes "

## save the changes
sudo netfilter-persistent save

log_info "Restart iptables service"
sudo systemctl restart iptables

log_success "Job ended successfully "