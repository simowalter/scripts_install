#!/bin/bash

#stop when encounter an errror
set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }

function install_docker(){
    log_info "Mise à jour des paquets et depot"
    sudo apt update
    sudo apt upgrade -y
    sudo apt-get install ca-certificates curl gnupg lsb-release -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor \
    -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo \
    "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y

    log_info "Installation de Docker-compose"
    sudo apt install docker-compose
    sudo adduser $USER docker
    sudo usermod -aG docker $USER

    log_info "Reload of deamon docker and restart of Docker"
    sudo systemctl daemon-reload
    sudo systemctl restart docker

    log_success "Installation de Docker terminée"
}

function install_docker_2(){
    log_info "Mise à jour des paquets et depot"
    sudo apt update
    sudo apt upgrade -y
    
    log_info " download the installation script from https://get.docker.com "
   
    curl -fsSL https://get.docker.com -o install-docker.sh
 
    log_info "run the script with --dry-run to verify the steps it executes"
    
    sh install-docker.sh --dry-run

    log_info " run the script either as root, or using sudo to perform the installation."
    
    sudo sh install-docker.sh

    log_info "Installation de Docker-compose"
    sudo apt install docker-compose
    sudo adduser $USER docker
    sudo usermod -aG docker $USER

    log_info "Reload of deamon docker and restart of Docker"
    sudo systemctl daemon-reload
    sudo systemctl restart docker

    log_success "Installation de Docker terminée"
}
echo -e "\n\033[34m ################################# This is a script to install Docker & Docker-compose ################################# \033[0m\n\n"

if command -v docker &> /dev/null; then
    log_info "Docker already installed "
    read -p "---------------- Do you want to purge and reinstall? type 'y' or 'n' : " response

    if [ "$response" = "y" ]; then
        #uninstall Docker & Docker-compose
        log_info "Uninstallation of installed docker (and docker-compose)"
        sudo apt-get remove docker docker-engine docker.io containerd runc -y
        sudo rm /usr/local/bin/docker-compose
        sudo apt remove docker-compose
        sudo apt autoremove
        install_docker
    else
        log_info "Goodbye 😀"
    fi
else
    install_docker
fi