#!/bin/bash

set -e

function log_info() {
      echo -e "\n[\\e[1;94mINFO\\e[0m] \033[34m -------------------------------> $*   \033[0m\n\n"
  }

function log_success() {
      echo -e "\n\e[30;42m[SUCCESS INFO] 😀😀 $*  😀😀  \e[0m \n\n"
  }


function install_harbor(){
    log_info "Ajout du repository Helm de Harbor Helm"
    helm repo add harbor https://helm.goharbor.io

    log_info "Récupère les derniers charts depuis le repository"
    helm repo update

    # log_info "Récupère et decompression de la derniere sersion depuis le repository"
    # helm fetch harbor/harbor --untar

    # log_info "editez le fichier values.yaml "
    # nano harbor/values.yaml

    read -s -p $'\nEnter adminPassword for HARBOR UI: ' ADMIN_PASSWORD_HARBOR_UI
    echo   # This adds a newline character

    log_info "Installation de harbor"
    helm install harbor harbor/harbor --namespace harbor --create-namespace --set expose.type=nodePort,expose.tls.auto.commonName=core.harbor.registry-walter.com,expose.ingress.hosts.core=harbor.registry-walter.com,expose.nodePort.ports.http.nodePort=30008,expose.nodePort.ports.https.nodePort=30443,externalURL="https://harbor.registry-walter.com:30443",persistence.persistentVolumeClaim.trivy.size="2Gi",harborAdminPassword=$ADMIN_PASSWORD_HARBOR_UI,trivy.resources.requests.cpu="100m",trivy.resources.requests.memory="128Mi",trivy.resources.limits.cpu="200m",trivy.resources.limits.memory="256Mi"

    log_info " Affichage des pods de Harbor "
    kubectl -n harbor get pod

    log_success "Installation de Harbor terminée. NB: Attendez que tous les pods soient prêt"
}
echo -e "\n\033[34m ################################# This is a script to install Harbor on k8s cluster ################################# \033[0m\n\n"
install_harbor


